# Roma nel Settecento / Rome in the 1700s

The dataset provided refers to [Renata Ago](mailto:renata.ago@uniroma1.it)'s study investigating urban mobility in 1700s Rome. The whole project was developed on [QGIS](http://qgis.org/) in collaboration with [Domizia D'Erasmo](mailto:domizia.derasmo@uniroma1.it). The data released were selected to allow replication of the methodology used in this research, being published:

- Ago, Renata, and Domizia D’Erasmo. 2021. ‘Analysis of Urban Mobility in 18th Century Rome: A Research Approach through GIS Platform’. In *Proceedings of ArcheoFOSS XIV: Open Software, Hardware, Processes, Data and Formats in Archaeological Research, 15-17 Ottobre 2020*, edited by J. Bogdani, E. Demetrescu, R. Montalbano and P. Rosati. Oxford: Archaeopress.

- Ago, Renata. 2021. *Il Diritto Alla Città. Roma Nel Settecento*. Roma: Viella.

The above listed bibliography provides full information about the project, the research results, and the source of the data.


## Data set structure


The data set is madeof two GeoJSON files containing 200 paths (paths_1739.geojson) and 445 stop-overs (stops_1739.geojson) of home-work and home-business itineraries from 1739.


### paths_1739.geojson
The file contains 200 home-work and home-business itineraies  of private citizens of Rome in 1739.  
The paths, vectorized as lines, are associated with the following attributes:

|Attribute|Description|
|---------|-----------|
|`fid`|Numeric Primary Key (PK)|
|`nome`<br>[name]|Given name of the person traveling the itinerary|
|`cognome`<br>[family name]|Family name of the person traveling the itinerary|
|`nome_cognome`<br>[name and surname]|Name and surname of the person traveling the itinerary. This is a sring PK and is referred by stops_1739.geojson. <br> NOTE: When a person travels more than one path, the name and surname are followed by an identifying number in brackets to make the value unique (e.g. Bartolomeo Battistelli (1); Bartolomeo Battistelli (2)).|
|`mestiere` <br>[job]|Job of the person traveling the path|
|`tipologia` <br>[typology]|Type of path: `casa-lavoro` (home-work) or `casa-affari` (home-business)|
|`anno` <br>[year]|Year in which the path was traveled. <br> NOTE: In the project, paths from the years 1739, 1748, and 1749 were collected. This dataset only shows the paths from 1739.|
|`distanza` <br>[distance]|The length, in meters, of the itinerary|


---


### stops_1739.geojson
The file contains 445 stop-overs of the home-work and home-business itineraries of private citizens of Rome in 1739. The stop-overs, vectorized as points, are associated with the following attributes:

|Attribute|Description|
|---------|-----------|
|`nome_cognome`<br>[name and surname]|Name and surname of the person walking the itinerary. This attribute contains the Foreign Key (FK) that refers to the strig PK of the paths file (paths_1739.geojson).|
|`mestiere` <br>[job]|Job of the person traveling the path|
|`tipologia` <br>[typology]|Type of path: `casa-lavoro` (home-work) or `casa-affari` (home-business)|
|`tappa`<br>[stop]|Name of each stop-over (streets, squares, palaces, churches, etc..)|
|`anno` <br>[year]|Year in which the path was traveled. <br> NOTE: In the project, paths from the years 1739, 1748, and 1749 were collected. This dataset only shows the paths from 1739.|
|`start_pause_stop`|Whether the stop is an initial, intermediate or final one|
|`anno`<br>[year]|year in which the path was covered. <br> NOTE: In the project, paths from the years 1739, 1748, and 1749 were collected. This dataset only shows the paths from 1739.|


---


## License and Copyright

Copyright by [Renata Ago](mailto:renata.ago@uniroma1.it) and [Domizia D'Erasmo](mailto:domizia.derasmo@uniroma1.it) 2020.

This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).


---


## Acknowledgement
This repositoryis maintained by [LAD: Laboratorio di Archeologia Digitale alla Sapienza](https://sites.google.com/uniroma1.it/lad).